Views Checkboxes

This module is used to change the form elements on view pages with filter forms.  Those forms normally use select boxes for single and multiple selection drop-down inputs.  When this module is installed and enabled, the single-selection drop-down can be changed to a list of radio buttons and the multi-select box to a list of checkboxes.

You can define a limit on number of entries in the filter options.  E.g. if the limit is set to 5 and there are 6 options, they will get displayed as a list.  If there are 5 or less options they will get displayed with checkboxes/radios.

Instructions:
Enable the module on the modules page and all users with the 'administer
views' permission will be able to access the admin page for
'Views Checkboxes' and enable it.  Right now, it is enabled site-wide or not
at all.
